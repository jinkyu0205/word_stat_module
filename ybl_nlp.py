import os
import re
import pickle
import json
import math
import time
import numpy as np
from tqdm import tqdm
from scipy.stats.mstats import gmean

def read_text_file(file_name):
    if file_name[-4:] != '.txt':
        file_name += '.txt'
    sentences = []
    try:
        f = open(file_name, 'r', encoding='utf8')
    except UnicodeDecodeError:
        try:
            f = open(file_name, 'r')
        except UnicodeDecodeError:
            raise Exception('Cannot open the file')
    while True:
        line = f.readline()
        if not line:
            break
        sentences.append(line)
    return sentences


def read_saved_file(file_name):
    if file_name[-4:] == '.pkl':
        with open(file_name, 'rb') as f:
            return pickle.load(f)
    elif file_name[-4:] == 'json':
        with open(file_name, 'rb') as f:
            return json.load(f)

def get_deepest(dictionary):
    output_keys = []
    dict_keys = list(dictionary.keys())
    subtree_key_length = len(dict_keys)
    while subtree_key_length > 1:
        output_keys.append(dict_keys[0])
        dict_keys = list(dictionary.keys())
        subtree_key_length = len(dict_keys)


# -----------------------------------------------------------

# -------------------------------------------------

class yblTokenizer:
    def __init__(self, score_data='none'):
        self.is_trained = {'forward': False, 'backward': False}
        if score_data == 'none':
            self.forward_word_freq = dict()
            self.backward_word_freq = dict()
            print('Initialized')
        else:
            retrieved = read_saved_file(score_data)
            self.forward_word_freq = retrieved['forward']
            self.backward_word_freq = retrieved['backward']
            del retrieved
            if self.forward_word_freq:
                self.is_trained['forward'] = True
            if self.backward_word_freq:
                self.is_trained['backward'] = True
            print('Retrieved')
        self.min_conf = 0.1
        self.pop_cur_ind = []
        self.pop_cur_tot_len = []
        self.josa = ['으로부터', '이야말로', '는커녕', '로부터', '새로에', '야말로', '에게서', '은커녕', '이나마',
                     '이든가', '이든지', '이시여', '이었다', '그래', '그려', '까지', '께서', '나마', '대로', '든가',
                     '든지', '따라', '라고', '마는', '마저', '밖에', '부터', '에게', '에다', '에서', '엔들', '였다',
                     '이나', '이다', '이란', '이랑', '이며', '이여', '인들', '인즉', '일랑', '조차', '처럼', '치고',
                     '커녕', '토록', '하고', '한테', '가', '고', '과', '께', '나', '는', '도', '들', '란', '랑', '를',
                     '만', '뿐', '서', '써', '아', '야', '에', '여', '와', '요', '은', '을', '의', '이', '즉']
        self.josa_next = list(set([josa[0] for josa in self.josa]))
        self.neg_terms = ['투자의견', '매수', '매수Buy', '매수BUY', '목표주가', '연결재무제표', '요약연간', '매출액',
                          '억원', '영업이익', '세전이익', '순이익지배', 'EPS', 'ROE', 'PER', 'PBR', 'EVEBITDA', 'SPS',
                          '유형자산', '무형자산', '자산총계', '유동부채', '유동자산', '판관비율', '영업이익률', 'EBITDA',
                          '영업외손익', '10매입채무', '금융이자손익', '10단기차입금', '비유동부채', '외화관련손익',
                          '기타영업외손익', '부채총계', '사채및장기차입금', '지배기업지분', '법인세비용', '자본잉여금',
                          '법인세율', '계속사업이익', '이익잉여금', '당기순이익', '세전이익률', '비지배지분', '자본총계',
                          '총차입금', '순차입금', '현금흐름표', '영업활동현금흐름', '당기순이익'
                          '그에', '있다', '있을', '있으', '있는', '있고', '있어', '있겠지', '있을지', '그에', '것이',
                          '되고', '되는', '되어', '되었다', '것으로', '이에']
        self.neg_sentence_terms = ['증권', '출처:', '기업:', '산업:', '투자등급', '자료에 게재된', '보유하고 있지',
                                   '이해관계가 없으며', '반영하고 있으나', '목적으로 하고 있습니다.',
                                   '판단으로 하시기', '재배포될 수 없습니다', '이 레포트에는', '고객지원센터', '리포트']


    def update_forward_word_frequency(self, word):
        reference_dict = self.forward_word_freq
        for idx, alpha in enumerate(word):
            if idx > 0:
                reference_dict = reference_dict[word[idx-1]]
            try:
                check = reference_dict[alpha]
                check['_freq'] += 1
            except KeyError:
                reference_dict[alpha] = {'_freq': 1}

    def update_backward_word_frequency(self, word):
        reference_dict = self.backward_word_freq
        word = word[::-1]
        for idx, alpha in enumerate(word):
            if idx > 0:
                reference_dict = reference_dict[word[idx - 1]]
            try:
                check = reference_dict[alpha]
                check['_freq'] += 1
            except KeyError:
                reference_dict[alpha] = {'_freq': 1}

    def update_frequency_dictionary(self, sentences):
        if len(sentences) > 50000:
            print('Updating the Forward/Backward Frequency Trees...')
            for sentence in tqdm(sentences):
                sentence = sentence.strip()
                sentence = sentence.split()

                for term in sentence:
                    if len(term) > 25:
                        continue
                    self.update_forward_word_frequency(term)
                    self.update_backward_word_frequency(term)
            print('\nTree update complete')
        else:
            for sentence in sentences:
                sentence = sentence.strip()
                sentence = sentence.split()

                for term in sentence:
                    if len(term) > 25:
                        continue
                    self.update_forward_word_frequency(term)
                    self.update_backward_word_frequency(term)



    def next_dict_order(self, standard, candidate):
        """
        Numerical한 dictionary order가 있을 때 다음 order가 무엇인지 알려주는 함수. 예를 들어
        standard가 [2, 3, 1, 2]이고 candidate이 [1,1,1,1] 이라면 다음 order는 [1, 1, 1, 2]
        그 다음은 [1, 2, 1, 1], 그 후 [1, 2, 1, 2], [1, 3, 1, 1], [1, 3, 1, 2], [2, 1, 1, 1]... 이런 식
        :param standard: Dictionary order의 기준
        :param candidate: 현재 다음 order를 알고 싶은 그 기점
        :return: standard하에서 candidate 다음의 oder
        """
        standard = standard[::-1]
        candidate = candidate[::-1]
        cand_length = len(candidate)

        idx = 0
        cand = candidate[idx]
        stand = standard[idx]
        while cand+1 > stand:
            idx += 1
            if idx>=cand_length:
                return [], True
            cand = candidate[idx]
            stand = standard[idx]
        candidate[idx] += 1
        del candidate[:idx]
        return candidate[::-1], False

    def convert_dict_key_idx_to_keys(self, key_idx, fwb='forward'):
        """
        Dictionary tree에서 key index를 string key로 변환시켜 준다.
        :param key_idx: key index
        :param fwb: forward/backward dictionary인지 결정
        :return:
        """
        if fwb == 'forward':
            temp_dict = self.forward_word_freq
        else:
            temp_dict = self.backward_word_freq

        output = []
        for idx in key_idx:
            curkeys = list(temp_dict.keys())
            try:
                curkeys.remove('_freq')
            except:
                pass

            curkey = curkeys[idx]
            output.append(curkey)
            temp_dict = temp_dict[curkey]
        return output

    def next_forward(self):
        """
        Dictionary cleansing 작업을 할 때 (forward) 현재 가지에서 다음 어디로 넘어가야 하는지 알려주는 함수.
        더 깊게 갈 수 있으면 깊게 가보고 더이상 깊게 갈 수 없으면 다음 Branch로 넘어간다.
        :return:
        """
        if len(self.pop_cur_ind) == 0:
            pop_keys = list(self.forward_word_freq.keys())
            self.pop_cur_ind = [0]
            self.pop_cur_tot_len = [len(pop_keys)-1]
            return [pop_keys[0]], False

        cur_dict = self.forward_word_freq
        cur_keys = self.convert_dict_key_idx_to_keys(self.pop_cur_ind, 'forward')
        for curkey in cur_keys:
            cur_dict = cur_dict[curkey]

        dict_keys = list(cur_dict.keys())
        dict_keys.remove('_freq')

        if len(dict_keys) == 0:
            # Dead end tree. Move to next
            next_dict_index, indicator = self.next_dict_order(self.pop_cur_tot_len, self.pop_cur_ind)
            del self.pop_cur_tot_len[len(next_dict_index):]
            self.pop_cur_ind = next_dict_index
            return self.convert_dict_key_idx_to_keys(self.pop_cur_ind, 'forward'), indicator

        else:
            # Go Deeper
            self.pop_cur_tot_len.append(len(dict_keys)-1)
            self.pop_cur_ind.append(0)
            return self.convert_dict_key_idx_to_keys(self.pop_cur_ind), False

    def next_backward(self):
        """
        Dictionary cleansing 작업을 할 때 (backward) 현재 가지에서 다음 어디로 넘어가야 하는지 알려주는 함수.
        더 깊게 갈 수 있으면 깊게 가보고 더이상 깊게 갈 수 없으면 다음 Branch로 넘어간다.
        :return:
        """
        if len(self.pop_cur_ind) == 0:
            pop_keys = list(self.backward_word_freq.keys())
            self.pop_cur_ind = [0]
            self.pop_cur_tot_len = [len(pop_keys)-1]
            return [pop_keys[0]], False

        cur_dict = self.backward_word_freq
        cur_keys = self.convert_dict_key_idx_to_keys(self.pop_cur_ind, 'backward')
        for curkey in cur_keys:
            cur_dict = cur_dict[curkey]

        dict_keys = list(cur_dict.keys())
        dict_keys.remove('_freq')

        if len(dict_keys) == 0:
            # Dead end tree. Move to next
            next_dict_index, indicator = self.next_dict_order(self.pop_cur_tot_len, self.pop_cur_ind)
            del self.pop_cur_tot_len[len(next_dict_index):]
            self.pop_cur_ind = next_dict_index
            return self.convert_dict_key_idx_to_keys(self.pop_cur_ind, 'backward'), indicator

        else:
            # Go Deeper
            self.pop_cur_tot_len.append(len(dict_keys)-1)
            self.pop_cur_ind.append(0)
            return self.convert_dict_key_idx_to_keys(self.pop_cur_ind, 'backward'), False


    def dict_cleaner(self, minimum_frequency=100):
        """
        최소한의 빈도를 정하여 가지수를 줄인다. Normalize는 하지 않는다.
        :param minimum_frequency: 최소한의 빈도 수
        :return:
        """
        self.pop_cur_ind = []
        self.pop_cur_tot_len = []

        ended = False
        next_index = []
        while not ended:
            cur_dict = self.forward_word_freq
            for key in next_index:
                cur_dict = cur_dict[key]

            cur_keys = list(cur_dict.keys())
            try:
                cur_keys.remove('_freq')
            except:
                pass

            for curkey in cur_keys:
                if cur_dict[curkey]['_freq'] < minimum_frequency:
                    del cur_dict[curkey]
            next_index, ended = self.next_forward()
        print('Forward done')

        ended = False
        next_index = []
        self.pop_cur_ind = []
        self.pop_cur_tot_len = []
        while not ended:
            cur_dict = self.backward_word_freq
            for key in next_index:
                cur_dict = cur_dict[key]

            cur_keys = list(cur_dict.keys())
            try:
                cur_keys.remove('_freq')
            except:
                pass

            for curkey in cur_keys:
                if cur_dict[curkey]['_freq'] < minimum_frequency:
                     del cur_dict[curkey]
            next_index, ended = self.next_backward()

    def process_current_dictionary(self, minimum_frequency=100):
        """
        정해진 frequency 보다 낮은 빈도로 나오는 단어 가지를 쳐내며 frequenc를 normalize시켜 확률로 만든다.
        :param minimum_frequency: 최소한의 단어 빈도 수
        :return:
        """
        ended = False
        next_index = []
        while not ended:
            cur_dict = self.forward_word_freq
            for key in next_index:
               cur_dict = cur_dict[key]

            cur_keys = list(cur_dict.keys())
            try:
                cur_keys.remove('_freq')
            except:
                pass

            freqs = []
            for curkey in cur_keys:
               if cur_dict[curkey]['_freq'] >= minimum_frequency:
                   freqs.append(cur_dict[curkey]['_freq'])
               else:
                   del cur_dict[curkey]

            cur_keys = list(cur_dict.keys())
            try:
                cur_keys.remove('_freq')
            except:
                pass
            freqs = sum(freqs)
            for curkey in cur_keys:
               cur_dict[curkey]['_freq'] = np.round(cur_dict[curkey]['_freq']/freqs, decimals=6)

            next_index, ended = self.next_forward()
        print('Forward done')

        ended = False
        next_index = []
        self.pop_cur_ind = []
        self.pop_cur_tot_len = []
        while not ended:
            cur_dict = self.backward_word_freq
            for key in next_index:
               cur_dict = cur_dict[key]

            cur_keys = list(cur_dict.keys())
            try:
                cur_keys.remove('_freq')
            except:
                pass

            freqs = []
            for curkey in cur_keys:
               if cur_dict[curkey]['_freq'] >= minimum_frequency:
                   freqs.append(cur_dict[curkey]['_freq'])
               else:
                   del cur_dict[curkey]

            cur_keys = list(cur_dict.keys())
            try:
                cur_keys.remove('_freq')
            except:
                pass
            freqs = sum(freqs)
            for curkey in cur_keys:
               cur_dict[curkey]['_freq'] = np.round(cur_dict[curkey]['_freq']/freqs, decimals=6)

            next_index, ended = self.next_backward()

    def save_model(self, file_name):
        print('Saving ' + file_name + '...')
        start = time.time()
        with open(file_name, 'w') as f:
            json.dump({'forward': self.forward_word_freq, 'backward': self.backward_word_freq}, f)
        print(file_name + 'successfully saved. Time Elapsed: ' + str(time.time()-start))


    def calc_entropy(self, term, direction='forward'):
        if direction == 'forward':
            cur_dict = self.forward_word_freq
        else:
            cur_dict = self.backward_word_freq

        try:
            for word in term:
                cur_dict = cur_dict[word]
        except KeyError:
            return -1

        cur_keys = list(cur_dict.keys())
        try:
            cur_keys.remove('_freq')
        except:
            pass

        entropy_of_conditional_prob = []
        for key in cur_keys:
            conditional_prob = cur_dict[key]['_freq']
            if conditional_prob <= 0:
                entropy = 0
            else:
                entropy = -conditional_prob * math.log(conditional_prob)
            entropy_of_conditional_prob.append(entropy)
        return sum(entropy_of_conditional_prob)

    def calc_cohesion(self, term):
        conditional_prob = []
        cur_dict = self.forward_word_freq
        try:
            for idx, char in enumerate(term):
                if idx == 0:
                    cur_dict = cur_dict[char]
                    continue
                cur_dict = cur_dict[char]
                conditional_prob.append(cur_dict['_freq'])
        except:
            return 0
        if len(conditional_prob) == 0:
            return 0
        return gmean(conditional_prob)



    def entropy_splitter(self, term):
        term_length = len(term)
        scores = []
        splits = []
        for split_index in range(term_length):
            split_index += 1

            term1 = term[:split_index]
            term2 = term[split_index:]
            splits.append([term1, term2])


            forward_ent = self.calc_entropy(term1, 'forward')
            backward_ent = self.calc_entropy(term1[::-1], 'backward')

            scores.append(max([forward_ent, backward_ent]))

        max_score_ind = np.argmax(scores)
        output = splits[max_score_ind]
        try:
            output.remove('')
        except:
            pass
        return output

    def comprehensive_splitter(self, term):
        term_length = len(term)
        scores = []
        cohesion_score = []
        splits = []
        for split_index in range(term_length):
            split_index += 1

            term1 = term[:split_index]
            term2 = term[split_index:]
            splits.append([term1, term2])


            forward_ent = self.calc_entropy(term1, 'forward')
            backward_ent = self.calc_entropy(term1[::-1], 'backward')
            scores.append(max([forward_ent, backward_ent]))
            # scores.append(forward_ent)
            cohesion_score.append(self.calc_cohesion(term1))

        ent_max_score_ind = np.argmax(scores)
        coh_max_score_ind = np.argmax(cohesion_score)

        if ent_max_score_ind == coh_max_score_ind:
            output = splits[ent_max_score_ind]
        else:
            norm_ent = np.sort((scores - np.mean(scores)))[-2:]
            if np.std(scores) != 0:
                norm_ent = norm_ent/np.std(scores)
            norm_coh = np.sort((cohesion_score - np.mean(cohesion_score)))[-2:]
            if np.std(cohesion_score) != 0:
                norm_coh = norm_coh/np.std(cohesion_score)


            ent_confidence = norm_ent[1] - norm_ent[0]
            coh_confidence = norm_coh[1] - norm_coh[0]

            if coh_confidence >= ent_confidence:
                output = splits[coh_max_score_ind]
            else:
                output = splits[ent_max_score_ind]
        try:
            output.remove('')
        except:
            pass
        return output


    def check_josa_possibility(self, word, jl):
        """
        word라는 단어 뒤에 조사가 붙는 빈도를 계산하여 주는 함수. 우선 분모는 1차원 적으로 현재 단어 뒤에 오는 통계적으로
        가능한 모든 단어들(1글자) 이고, 분자는 현재 단어 뒤에 통계적으로 붙는 조사의 경우의 수 이다.
        :param word: 단어 candidate
        :param jl: 뒤에 붙을 조사의 길이를 설정. 0이면 모든 길이의 조사를 다룬다.
        :return: 조사 빈도 수
        """
        cur_dict = self.forward_word_freq
        for char in word:
            try:
                cur_dict = cur_dict[char]
            except KeyError:
                return 0

        tot_child_nodes = len(cur_dict.keys()) - 1  # _freq라는 key 때문에 -1
        freqs = []
        josa_freqs = []
        for key in list(cur_dict.keys()):
            if key == '_freq':
                continue
            freqs.append(cur_dict[key]['_freq'])
            if key in self.josa_next:
                josa_freqs.append(cur_dict[key]['_freq'])
                continue

        if len(freqs) == 0:
            freqs =[0]
        if len(josa_freqs) == 0:
            josa_freqs = [0]
        if np.mean(freqs) > 0.7 or np.sum(josa_freqs)< 0.2:  # 수정 필요. 목표주가 처럼 무조건 조사와 동일하게 '가'로 끝나는 경우
            return 0             #  variation이 적어 조사로 취급 안하게 하는 부분.
        josa_counter = 0
        if jl == 0:
            josa_jl = self.josa
        else:
            josa_jl = [josa for josa in self.josa if len(josa) == jl]

        for josa in josa_jl:
            temp_dict = cur_dict
            jsl = len(josa)
            for idx, char in enumerate(josa):
                try:
                    temp_dict = temp_dict[char]
                    if idx == jsl - 1:
                        josa_counter += 1
                except KeyError:
                    break

        if tot_child_nodes == 0:
            return 0
        return josa_counter/tot_child_nodes


    def josa_splitter(self, original_term):
        """
        단어에 붙어있는 조사를 판별하여 분리해 주는 함수 입니다.
        :param original_term: 조사 유무를 판별하기 위한 term (예: OOO에게, 주식이, 조씨일가...)
        :return: 만약 조사로 판별이 되면 list of strings로 단어, 조사 분리. 조사가 없다고 판별 되면 term이 그대로 나온다.
        """
        if len(original_term) <=2:
            return original_term

        josa_lengths = list(set([len(josa) for josa in self.josa]))
        josa_lengths.sort()
        josa_lengths = josa_lengths[::-1]


        term = original_term
        confidences = []
        splits = []
        cohesion = []
        for jl in josa_lengths:
            if jl >= len(term):
                continue
            word1 = term[:-jl]
            candidate = term[-jl:]
            if not candidate in self.josa:
                continue
            confidence = self.check_josa_possibility(word1, 0)
            word_cohesion = self.calc_cohesion(word1)
            if confidence > 0:
                splits.append([word1, term[-jl:]])
                confidences.append(confidence)
                cohesion.append(word_cohesion)

        if len([num for num in confidences if num > self.min_conf]) > 0:
            max_idx = [i for i, item in enumerate(confidences) if item > self.min_conf]
            return splits[max_idx[0]]
        return original_term


    def financial_term_filter(self, term):
        # Check if date -----------------------------------------
        output = []
        output += re.findall(r'\d{2}년', term)
        output += re.findall(r'\d월', term)
        output += re.findall(r'\d일', term)
        output += re.findall(r'\d시', term)
        output += re.findall(r'\d분', term)
        output += re.findall(r'\d초', term)
        if output:
            return ''
        # Check if money amount -----------------------------------
        output += re.findall(r'\d원', term)
        output += re.findall(r'\d\w원', term)
        output += re.findall(r'\d달러', term)
        output += re.findall(r'\d\w달러', term)

        # Check if stock counts ----------------------------------
        output += re.findall(r'\d주', term)  # 주식 갯수 포함
        output += re.findall(r'\d\w주', term)  # 주식 갯수 포함
        if output:
            return ''
        if term in self.neg_terms:
            return ''
        return term

    def financial_sentence_filter(self, sentences):
        if isinstance(sentences, str):
            sentences = [sentences]

        output = []
        only_lang = re.compile('[^\W\d]')
        for sentence in sentences:
            if len(''.join(only_lang.findall(sentence))) < 10:
                continue

            neg_indicator = False
            for neg_sent_word in self.neg_sentence_terms:
                if neg_sent_word in sentence:
                    neg_indicator = True
                    break
            if neg_indicator:
                continue
            output.append(sentence)
        return output


# These functions do not belong to the module ---------------------------------------------------
def train_from_namu_wiki():
    tokenizer_module = yblTokenizer('./trained/namu_wiki_raw.json')
    for i in range(47):
        dat_file_name = './data/namu_wiki/data_' + str(i + 1) + '.txt'
        if i == 0:
            tokenizer_module = yblTokenizer()
        else:
            tokenizer_module = yblTokenizer('./trained/namu_wiki_raw.json')

        sentence = read_text_file(dat_file_name)
        tokenizer_module.update_frequency_dictionary(sentence)

        if i % 4 == 0:
            tokenizer_module.dict_cleaner(5)
        tokenizer_module.save_model('./trained/namu_wiki_raw.json')
        print('Update upto ' + str(i + 1) + '-th data file complete')

    tokenizer_module.process_current_dictionary(10)
    print('process done. now normalizing...')
    tokenizer_module.save_model('./trained/namu_wiki_normalized.json')

def train_from_stock_reports():
    file_dirs = []
    base_root = './data/stock_reports'

    for folder_level1 in os.listdir(base_root):
        for folder_level2 in os.listdir(base_root + '/' + folder_level1):
            for folder_level3 in os.listdir(base_root + '/' + folder_level1 + '/' + folder_level2):
                cur_dir = base_root + '/' + folder_level1 + '/' + folder_level2 + '/' + folder_level3
                for fname in os.listdir(cur_dir):
                    if fname[-3:] == 'txt':
                        file_dirs.append(cur_dir + '/' + fname)

    only_language = re.compile('[^\W\d]')
    language_and_num = re.compile('[\w]')
    print('Training Begins...')
    for i, dat_name in tqdm(enumerate(file_dirs)):
        if not 'tokenizer_module' in vars():
            if i == 0:
                tokenizer_module = yblTokenizer('./trained/namu_wiki_raw.json')
            else:
                tokenizer_module = yblTokenizer('./trained/ybl_word_freq_counts.json')

        sentence = read_text_file(dat_name)
        sent = ''
        for line in sentence:
            if len(line) < 30:
                sent += line.replace('\n', ' ')
            else:
                sent += line.replace('\n', '')
        sentence = ' '.join(sent.split('.'))
        sentence = sentence.split(' ')

        sentence = [''.join(language_and_num.findall(term)) for term in sentence if len(only_language.findall(term)) > 0]
        tokenizer_module.update_frequency_dictionary(sentence)

    print('cleaning...')
    start = time.time()
    tokenizer_module.dict_cleaner(20)
    print('cleaned: ', time.time() - start)
    print('saving...')
    start = time.time()
    tokenizer_module.save_model('./trained/ybl_word_freq_counts.json')
    print('Data' + str(i) + ' saved: ', time.time() - start)
    print('process done. now normalizing...')
    tokenizer_module.process_current_dictionary(30)
    tokenizer_module.save_model('./trained/ybl_word_normalized.json')

def train_from_txtfile(fname):
    base_root = './data/'

    only_language = re.compile('[^\W\d]')
    language_and_num = re.compile('[\w]')
    print('Training Begins...')
    tokenizer_module = yblTokenizer('./trained/ybl_word_freq_counts.json')

    sentence = read_text_file(base_root + fname)
    sent = ''
    for line in tqdm(sentence):
        if len(line) < 30:
            sent += line.replace('\n', ' ')
        else:
            sent += line.replace('\n', '')
    sentence = ' '.join(sent.split('.'))
    sentence = sentence.split(' ')

    sentence = [''.join(language_and_num.findall(term)) for term in sentence if len(only_language.findall(term)) > 0]
    tokenizer_module.update_frequency_dictionary(sentence)

    print('cleaning...')
    start = time.time()
    tokenizer_module.dict_cleaner(10)
    print('cleaned: ', time.time() - start)
    print('saving...')
    start = time.time()
    tokenizer_module.save_model('./trained/ybl_word_freq_counts.json')
    print('process done. now normalizing...')
    tokenizer_module.process_current_dictionary(30)
    tokenizer_module.save_model('./trained/ybl_word_normalized.json')

def stock_report_tokenizer():
    base_root = './data/stock_reports'
    target_root = './data/stock_reports_processed'
    only_language = re.compile('[^\W\d]')
    language_and_num = re.compile('[\w]')
    for folder_level1 in tqdm(os.listdir(base_root)):
        if not os.path.exists(target_root + '/' + folder_level1):
            os.mkdir(target_root + '/' + folder_level1)
        for folder_level2 in os.listdir(base_root + '/' + folder_level1):
            if not os.path.exists(target_root + '/' + folder_level1 + '/' + folder_level2):
                os.mkdir(target_root + '/' + folder_level1 + '/' + folder_level2)
            for folder_level3 in os.listdir(base_root + '/' + folder_level1 + '/' + folder_level2):
                cur_dir = base_root + '/' + folder_level1 + '/' + folder_level2 + '/' + folder_level3
                target_dir = target_root + '/' + folder_level1 + '/' + folder_level2 + '/' + folder_level3
                for fname in os.listdir(cur_dir):
                    dat_name = cur_dir + '/' + fname
                    if not 'tokenizer_module' in vars():
                            tokenizer_module = yblTokenizer('./trained/ybl_word_normalized.json')

                    sentence = read_text_file(dat_name)
                    sent = ''
                    for line in sentence:
                        if len(line) < 30:
                            sent += line.replace('\n', ' ')
                        else:
                            sent += line.replace('\n', '')
                    sentence = tokenizer_module.financial_sentence_filter(sent.split('.'))
                    sentence = ' '.join(sentence)
                    sentence = sentence.split(' ')

                    sentence = [''.join(language_and_num.findall(term)) for term in sentence if
                                len(only_language.findall(term)) > 0]
                    output = []
                    for term in sentence:
                        term = tokenizer_module.financial_term_filter(term)
                        if len(term) <= 1:
                            continue
                        processed = tokenizer_module.josa_splitter(term)
                        if isinstance(processed, str):
                            output.append(processed)
                        else:
                            if len(processed[0]) > 1:
                                output.append(processed[0])
                    if not os.path.exists(target_dir):
                        os.mkdir(target_dir)
                    with open(target_dir + '/' + fname, 'w', encoding='utf8') as f:
                        for word in output:
                            f.write(word + '\n')


if __name__ == '__main__':
    # train_from_namu_wiki() # This trains the model from namu wiki data
    # train_from_stock_reports()
    # train_from_txtfile('naver_news_moneytoday_20190501_20190523.txt')

    # stock_report_tokenizer() # Working on it


    # Test Tokenizer ----------------------------------------------------------------
    # tokenizer_module = yblTokenizer('./trained/namu_wiki_normalized.json')

    tokenizer_module = yblTokenizer('./trained/ybl_word_normalized.json')

    print(tokenizer_module.josa_splitter('하이닉스에게'))
    print(tokenizer_module.josa_splitter('노란색은'))
    print(tokenizer_module.josa_splitter('아이오아이가'))
    print(tokenizer_module.josa_splitter('삼성전자를'))
    print(tokenizer_module.josa_splitter('케네디가'))
    print(tokenizer_module.josa_splitter('진규에게'))
    print(tokenizer_module.josa_splitter('주가가'))
    print(tokenizer_module.josa_splitter('주식이'))
    print(tokenizer_module.josa_splitter('애플은'))
    print(tokenizer_module.josa_splitter('호텔신라가'))
    print(tokenizer_module.josa_splitter('정제마진이'))
    print(tokenizer_module.josa_splitter('디스플레이'))

    print(tokenizer_module.josa_splitter('주가'))
    print(tokenizer_module.josa_splitter('삼성일가'))
    print(tokenizer_module.josa_splitter('웨이상이'))
    print(tokenizer_module.josa_splitter('sk하이닉스가'))
    #
    print(tokenizer_module.josa_splitter('일대일로의'))
    print(tokenizer_module.josa_splitter('대통령의'))
    print(tokenizer_module.josa_splitter('트럼프는'))
    print(tokenizer_module.josa_splitter('아마존은'))
    print(tokenizer_module.josa_splitter('수출되는'))
    print(tokenizer_module.josa_splitter('감정평가'))
    print(tokenizer_module.josa_splitter('실거래가'))
    print(tokenizer_module.josa_splitter('거래가'))



    # print(tokenizer_module.comprehensive_splitter('정류장은'))
    # print(tokenizer_module.comprehensive_splitter('노란색은'))
    # print(tokenizer_module.comprehensive_splitter('아이오아이가'))
    # print(tokenizer_module.comprehensive_splitter('삼성전자를'))
    # print(tokenizer_module.comprehensive_splitter('sk하이닉스가'))
    # print(tokenizer_module.comprehensive_splitter('케네디가'))
    # --------------------------------------------------------------------------------