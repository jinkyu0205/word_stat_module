from konlpy.tag import Okt
import re
okt = Okt()
#
# print(okt.pos('나는 밥이 제일 좋다'))
# print(okt.pos('최근 실적이 제일 좋은 주식 알려줘'))
# print(okt.pos('오일선이 21선을 넘은 경우'))
# print(okt.morphs('나는 밥이 제일 좋다'))
# print(okt.morphs('오일선이 21선을 넘은 경우'))

time_set = ['최근', '근래', '요즘', '옛날', '내일', '오늘', '어제', '그제', '그저께']
modifier = ['제일', '최고', '가장', '조금', '많이', '좀', '아주', '살짝', '겨우']
def is_time_related(term):
    output = []
    output += re.findall(r'\d년', term)
    output += re.findall(r'\d월', term)
    output += re.findall(r'\d일', term)
    if len(output) > 0:
        return True
    if term in time_set:
        return True
    return False

def check_modifier(term):
    if term in modifier:
        return True
    return is_time_related(term)


def find_intent(sentence):
    org_sentence = okt.pos(sentence)
    mod_ind = [i for i, item in enumerate(org_sentence) if check_modifier(item[0])]

    sentence = []
    recent_mod_idx = [-10, -10]
    for i, item in enumerate(org_sentence):
        if i in mod_ind:
            del recent_mod_idx[0]
            recent_mod_idx.append(i)
        if i > 0:
            if i-1 in mod_ind:
                org_sentence[i] = (org_sentence[i-1][0] + ' ' + item[0], org_sentence[i][1])
                sentence.append((org_sentence[i-1][0] + ' ' + item[0], item[1]))
                if recent_mod_idx[1] - recent_mod_idx[0] == 1:
                    del sentence[-1]
                continue
        if i in mod_ind:
            continue
        sentence.append(item)

    josa_index = [i for i, item in enumerate(sentence) if item[1] == 'Josa']
    cand_groups = []
    for i, jos_idx in enumerate(josa_index):
        if i > 0:
            cand_groups.append(sentence[josa_index[i]-1:jos_idx])
        else:
            cand_groups.append(sentence[:jos_idx])
    print('cand')
    return

def test(sentence):
    sentence = okt.pos(sentence)

find_intent('최근 실적이 제일 좋은 주식 알려줘')
find_intent('어제 가장 많이 오른 종목')